<%--
  Created by IntelliJ IDEA.
  User: Serum
  Date: 19.01.2020
  Time: 18:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Task List</title>

    <link href="<c:url value="/res/style.css"/>" rel="stylesheet" type="text/css"/>


<%--    Список Задач--%>



</head>
<body>

<table class="style">
    <c:if test="${tasksList.size() > 0}">
        <tr>
            <th class="left-container">№</th>
            <th style="width:auto">Task Name</th>
            <th style="width: auto"> Edit Task</th>
            <th style="width: auto"> Delete Task</th>
        </tr>
        <c:forEach var="tasks" items="${tasksList}" varStatus="i">
            <tr>
                <td>${i.index+1} </td>
                <td>${tasks.taskName}</td>
                <td><a href="<c:url value = "/edit/${tasks.id}"/>">edit</a></td>
                <td><a href="<c:url value = "/delete/${tasks.id}"/>">delete</a></td>
            </tr>
        </c:forEach>
    </c:if>
</table>

<h2>Add</h2>
<c:url value="/add" var="addTask"/>
<a href="${addTask}">Add new Task</a>

<br>
<br>


</body>
</html>
