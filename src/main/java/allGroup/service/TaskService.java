package allGroup.service;

import allGroup.model.Task;

import java.util.List;

public interface TaskService {
    List<Task> allTask();
    void add(Task task);
    void delete(Task task);
    void edit(Task task);
    Task getById(int id);
}
