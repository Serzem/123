package allGroup.model;

import javax.persistence.*;


@Entity
@Table(name = "tasklist")
public class Task {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "taskName")
    private String taskName;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }


    @Override
    public String toString() {
        return id + " " + taskName;
    }
}
