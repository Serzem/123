package allGroup.DAO;

import allGroup.model.Task;

import java.util.List;

public interface TaskDAO {
    List<Task> allTask();
    void add(Task task);
    void delete(Task task);
    void edit(Task task);
    Task getById(int id);
}
