package allGroup.controller;

import allGroup.model.Task;
import allGroup.service.TaskService;
import com.sun.istack.internal.NotNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.soap.SOAPBinding;
import java.util.*;

@Controller
public class TaskController {
    private TaskService taskService;


    @Autowired
    @NotNull
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView allTask(@RequestParam(defaultValue = "1") int page) {
        List<Task> tasks = taskService.allTask();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("title");
        modelAndView.addObject("tasksList", tasks);
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addTask(@ModelAttribute("message") String message) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("addTask");
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addTask(@ModelAttribute("task") Task task) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        taskService.add(task);
        return modelAndView;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteTask(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        Task task = taskService.getById(id);
        taskService.delete(task);
        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView pageEditTask(@PathVariable("id") int id, @ModelAttribute("message") String message) {
        Task task = taskService.getById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editTask");
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public ModelAndView endEditTask(@PathVariable("id") int id,
            @ModelAttribute("task") Task task) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        taskService.edit(task);
        return modelAndView;
    }

}
